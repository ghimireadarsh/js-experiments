//exercise 4
var numbers = [1,2,3,4];

 function transform(collection, transformationFunction){
    var out = [];
    for (var i=0; i<collection.length; i++){
        out.push(transformationFunction(collection[i]));
    }
    return out;
 }

var output = transform(numbers,function(num){
    return num*2;
});
console.log(output);