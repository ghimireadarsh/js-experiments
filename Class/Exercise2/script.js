var defineMyself = {
    Name: "Adarsh Ghimire",
    Address:[
        {   status : "permanent",
            city : "Dhangadhi",
            district : "Kailali",
            country : "Nepal",
        },
        {   status : "Temporary", 
            city:"Kuleshwor",
            district:"Kathmandu",
            country:"Nepal",
        }
    ],
    Interest:[
        "Cricket",
        "Football",
        "TT",
        "Trekking",
    ],
    Education:[
        {
            level:"SLC",
            organization: "Jyoti English Boarding High School",
            passedYear:2068,
        },
        {
            level:"HSEB",
            organization: "United Academy",
            passedYear:2070,
        },
        {
            level:"Undergraduate",
            organization: "Jyoti English Boarding High School",
            Year:2071,
            passedYear: 2075,
        },
    ]
};
console.log(defineMyself);