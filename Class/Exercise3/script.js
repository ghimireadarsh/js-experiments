//Exercise 3 
var fruits=[
    {id:1, name:'apple', color:'red'},
    {id:2, name:'banana', color:'yellow'},
    {id:3, name:'pineapple', color:'brown'},
    {id:4, name:'watermelon', color:'green'},
    {id:5, name:'mango', color:'yellow'},
    {id:6, name:'grapes', color:'green'},
    
];
//search by id
function searchById(collection, id){
    for(var i = 0 ; i < collection.length; i++){
        if(collection[i].id==id){
            return collection[i].name;
        }
    }
}
var x=[];
for(var i = 1 ; i < fruits.length; i++){
    var b=searchById(fruits,i);
    x.push(b);
}
console.log(x);