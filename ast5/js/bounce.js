var container = document.getElementById("container");
var ants=[];
var antNumber=10;
var scoreCount = 0;
var ScoreWrapper;
var inputElement;
function CreateScoreBoard() {
    ScoreWrapper = document.getElementById('score-wrapper');
    ScoreWrapper.style.marginBottom = '10px';
    ScoreWrapper.style.marginTop = '10px';
    ScoreWrapper.style.textAlign = 'center';
    inputElement = document.createElement("input");
    inputElement.type = "text";
    inputElement.style.fontSize = '22px';
    inputElement.style.width = '40px'
    inputElement.style.backgroundColor = 'Green';
    inputElement.style.border = 'none';
    ScoreWrapper.appendChild(inputElement);
}
function createBall() {
    for(var i = 0; i < antNumber; i++){
        var temp = document.createElement('div');
        var top = Math.floor(Math.random() * 371);
        var left = Math.floor(Math.random() * 931);

        temp.style.width = 30+'px';
        temp.style.height = 30+'px';
        temp.style.backgroundColor = 'red';
        temp.style.position = 'absolute';
        temp.style.borderRadius  = '50%';
        temp.style.cursor = 'pointer';
        temp.setAttribute('y',top);
        temp.setAttribute('x',left);
        temp.style.top = top;
        temp.style.left = left;
        temp.setAttribute('scoreCount',scoreCount);
        temp.onclick = (function( index ) {
            
            return function() {
                index.parentNode.removeChild( index );
                scoreCount++;
                
            }
        })( temp );
        ants.push(temp);
        container.appendChild( temp );
    }
}

var positionTop = Math.floor(Math.random() * 371);
var positionLeft = Math.floor(Math.random() * 931);
var topValue = positionTop;
var leftValue = positionLeft;
var leftFlag =  Math.floor(Math.random() * 1); //0;
var topFlag =  Math.floor(Math.random() * -1); //0;
setInterval(function(){
    ants.forEach((singleAnt)=>{
        topValue = parseInt(singleAnt.getAttribute('y'));
        leftValue = parseInt(singleAnt.getAttribute('x'));
        topFlag = parseInt(singleAnt.getAttribute('topFlag'));
        leftFlag = parseInt(singleAnt.getAttribute('leftFlag'));
    
        if(topFlag == -1){
            if(topValue != 370) {
            topValue++;
            singleAnt.style.top = topValue + 'px';
            topFlag = -1;
            }
            else{
            topFlag = 1;
            }
        }
        else{
            if(topValue != 1){
                topValue--;
                singleAnt.style.top = topValue + 'px';
                topFlag = 1;
            }
            else{
                topFlag = -1;
                }
        }
   
        if(leftFlag == -1){
            if(leftValue != 930){
                leftValue++;
                singleAnt.style.left = leftValue + 'px';
                leftFlag = -1;
            }
            else{ 
                leftFlag = 1;
            }
        }
        else{
            if(leftValue != 1){
                leftValue--;
                singleAnt.style.left = leftValue + 'px';
                leftFlag = 1;
            }
            else{
                leftFlag = -1;
            }
        }
        singleAnt.setAttribute('y',topValue);
        singleAnt.setAttribute('x',leftValue);
        singleAnt.setAttribute('topFlag',topFlag);
        singleAnt.setAttribute('leftFlag',leftFlag);
        collision();
        
    });
    inputElement.value=parseInt(scoreCount);
}, 10 );
createBall();
CreateScoreBoard();
//var c = 0;
function collision(){
    ants.forEach((singleAnt)=>{
        leftValue = parseInt(singleAnt.getAttribute('x'));
        topValue = parseInt(singleAnt.getAttribute('y'));
        topFlag = parseInt(singleAnt.getAttribute('topFlag'));
        leftFlag = parseInt(singleAnt.getAttribute('leftFlag'));
        var ant1X = topValue + 15;
        var ant1Y = leftValue + 15;
        ants.forEach((singleAnt2)=>{
            leftValue2 = parseInt(singleAnt2.getAttribute('x'));
            topValue2 = parseInt(singleAnt2.getAttribute('y'));
            topFlag2 = parseInt(singleAnt2.getAttribute('topFlag'));
            leftFlag2 = parseInt(singleAnt2.getAttribute('leftFlag'));
            var ant2X = topValue2 + 15;
            var ant2Y = leftValue2 + 15;
            if(singleAnt != singleAnt2){
                var dist = Math.sqrt(Math.pow(ant2X- ant1X ,2) + Math.pow(ant2Y- ant1Y,2));
                //console.log(ant1X,ant1Y,ant2X,ant2Y,dist);
                if(dist<30){
                    //c++;  
                    //console.log(c);
                    topFlag = -1*topFlag;
                    leftFlag = -1*leftFlag;
                    topFlag2 = -1*topFlag2;
                    leftFlag2 = -1*leftFlag2;
                    // topValue = topValue - 15;
                    // topValue2 = topValue2 -15 ;
                    // leftValue = leftValue - 15;
                    // leftValue2 = leftValue2 - 15;
                    
                    singleAnt.setAttribute('topFlag',topFlag);
                    singleAnt.setAttribute('leftFlag',leftFlag);
                    singleAnt2.setAttribute('topFlag',topFlag2);
                    singleAnt2.setAttribute('leftFlag',leftFlag2);
                    
                    // singleAnt.setAttribute('topValue',topValue);
                    // singleAnt2.setAttribute('topValue',topValue2);                  
                    // singleAnt.setAttribute('leftValue',leftValue);
                    // singleAnt2.setAttribute('leftValue',topValue2);
                    
                 }
                 
            }
        });
    });
   
}


