const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");
canvas.width = 288;
canvas.height = 512;

const background = new Image();
const bird = new Image();
const ground = new Image();
const upperPipe = new Image();
const lowerPipe = new Image();
const gameOver = new Image();

background.src = "images/background-night.png";
bird.src = "images/bluebird-upflap.png";
ground.src = "images/base.png";
upperPipe.src = "images/pipeup.png";
lowerPipe.src = "images/pipedown.png";
gameOver.src = 'images/gameover.png';

// console.log("ground height", ground.height);
// console.log("canvas height", canvas.height);
// console.log("bird height", bird.height);
// console.log("bird width", bird.width);
// console.log("upper pipe height", upperPipe.height);

const gap = 100; //gap between the pipes

let upperPipeHeight = 242;
// let upperPipeWidth = 52;

const lowerPipeY = (upperPipe.height || upperPipeHeight) + gap;
console.log(upperPipe.height, lowerPipeY)

// console.log("lower pipe positiom", lowerPipeY);

const birdX = 10; // bird at x position
let birdY = 150; // bird at y position
const gravity = 1.5; //bird falling down speed

document.addEventListener('keydown', moveUp);

function moveUp() {
  birdY -= 35; //on key press move bird up by 35px
  fly.play();
}

//pipe coordinates
const pipe = [];

pipe[0] = {
  x: canvas.width,
  y: 0
};

//score
let score = 0;

//audio
var fly = new Audio();
const points = new Audio();
fly.src = 'sounds/fly.mp3';
points.src = 'sounds/score.mp3';

function draw() {
  c.drawImage(background, 0, 0); //draw background

  for (let i = 0; i < pipe.length; i++) {
    c.drawImage(upperPipe, pipe[i].x, pipe[i].y); //upper pipe
    c.drawImage(lowerPipe, pipe[i].x, pipe[i].y + lowerPipeY); //lowerpipe
    pipe[i].x--;

    if (pipe[i].x == 125) {
      pipe.push({
        x: canvas.width,
        y: Math.floor(Math.random() * upperPipe.height) - upperPipe.height
      });
    }

    //collision
    if (birdX + bird.width >= pipe[i].x &&
        birdX <= pipe[i].x + upperPipe.width &&
        (birdY <= pipe[i].y + upperPipe.height ||
          birdY + bird.height >= pipe[i].y + lowerPipeY) ||
      birdY + bird.height >= canvas.height - ground.height)
    {

      c.drawImage(gameOver, 50, 100);
      
      setTimeout(() => {
        location.reload();
      }, 500);
    }
    if (pipe[i].x == 5) {
      score++;
      points.play();
      //console.log(score);
    }
  }

  c.drawImage(ground, 0, canvas.height - ground.height);
  c.drawImage(bird, birdX, birdY);
  birdY += gravity; // pulls the bird down
  c.fillStyle = '#000';
  c.font = '20px Verdana';
  c.fillText(`Score : ${score}`, 95, canvas.height-20);
  requestAnimationFrame(draw);
}
draw();


