// var canvas = document.querySelector('canvas');
// canvas.height = 480;
// canvas.width = 270;
var myGameArea = {
  canvas: document.createElement("canvas"),
  initialize: function() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.context = this.canvas.getContext("2d");
    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
	this.interval = setInterval(updateGameArea, 20);
	window.addEventListener('keydown' ,function ( e ){
		myGameArea.key = e.keyCode;
	} );
	window.addEventListener('keyup' ,function ( e ){
		myGameArea.key = false;
	} );
  },
  clear: function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
};
function component(width, height, color, x, y) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;
  this.speedX = 0;
  this.speedY = 0;
  this.update = function() {
    var ctx = myGameArea.context;
    ctx.fillStyle = color;
    ctx.fillRect(this.x, this.y, this.width, this.height);
  };
  this.newPos = function() {
    this.x += this.speedX;
    this.y += this.speedY;
  };
}
function startGame() {
	myGameArea.initialize();
	redGamePiece = new component(80, 50, "red", 500, 500);
  }

function updateGameArea() {
  myGameArea.clear();
  //   redGamePiece.x +=1;
 
  if(redGamePiece.key ==37){redGamePiece.speedX = -1};
  if(redGamePiece.key ==39){redGamePiece.speedX = 1};
  if(redGamePiece.key ==38){redGamePiece.speedY = -1};
  if(redGamePiece.key ==40){redGamePiece.speedY = +1};
  redGamePiece.newPos();
  redGamePiece.update();
}

function moveUp() {
	redGamePiece.speedY -=10;
}
function moveDown() {
	redGamePiece.speedY +=10;
}
function moveLeft() {
	redGamePiece.speedX -=10;
}
function moveRight() {
	redGamePiece.speedX +=10;
}
function clearMove(){
	redGamePiece.speedX = 0;
	redGamePiece.speedY = 0;
}

