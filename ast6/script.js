var canvas = document.querySelector("canvas");
canvas.width = 246;
canvas.height = 800;

var context = canvas.getContext("2d");

var carImage = new Image();
var roadImage = new Image();
var enemyImage = new Image();

carImage.src = "images/player.png";
roadImage.src = "images/road.png";
enemyImage.src = "images/enemy.png";

var speed = 1;
var score = 0;
var enemyArray = [4, 84, 170];

function background(xPos, yPos) {
  this.xPos = xPos;
  this.yPos = yPos;
  this.dy = 1;

  this.showRoad = function() {
    context.drawImage(roadImage, this.xPos, this.yPos);
  };

  this.speedUpdate = function(speed) {
    this.dy = speed;
  };

  this.updateBackground = function() {
    this.showRoad();
    if (this.yPos >= 750) {
      this.yPos = -800;
    }
    this.yPos += this.dy;
  };
}

function loadCar() {
  this.xPos = 4;
  this.yPos = 600;
  this.showCar = function() {
    context.drawImage(carImage, this.xPos, this.yPos);
  };

  this.positionLeft = function() {
    if (this.xPos > 75 && this.xPos < 85) {
      this.xPos = 4;
    }

    if (this.xPos >= 170 && this.xPos < 180) {
      this.xPos = 84;
    }
  };

  this.positionRight = function() {
    if (this.xPos >= 78 && this.xPos < 85) {
      this.xPos = 170;
    }
    if (this.xPos >= 0 && this.xPos < 10) {
      this.xPos = 84;
    }
  };
}

function createEnemy() {
  this.xPos = enemyArray[Math.floor(Math.random() * enemyArray.length)];
  console.log(this.xPos);
  this.yPos = 70;
  this.dy = 1;

  this.speedUpEnemy = function(speed) {
    this.dy = speed + 2;
  };

  this.showEnemy = function() {
    context.drawImage(enemyImage, this.xPos, this.yPos);
  };

  this.enemyUpdate = function() {
    this.showEnemy();
    this.yPos += this.dy;
    if (this.yPos >= canvas.height) {
      this.xPos = enemyArray[Math.floor(Math.random() * enemyArray.length)];
      this.yPos = -(canvas.height - 600);
    }
  };
}

function checkCollision(enemy, player) {
  if (
    enemy.yPos + 145 > player.yPos &&
    enemy.xPos + 20 > player.xPos &&
    player.yPos + 145 > enemy.yPos &&
    player.xPos + 20 > enemy.xPos
  ) {
    location.reload();
  }
}

function scoreUpdate() {
  score += speed/10; 
  context.font = '18px Sans-serif';
  context.fillStyle = 'rgb(242, 177, 52)';
  context.fillText("SCORE : " + Math.floor(score), 70, canvas.height - 20);
  context.fill();
  context.fillText("Speed : " + Math.floor(speed*5) + "km/hr", 70, 20);
  ontext.fill();
}

var initialBackground = new background(0, 0);
var finalBackground = new background(0, -790);
var player = new loadCar();
var enemy = new createEnemy();

function keyPressed(key) {
  if (key.keyCode == 38) { //up key
    speed += 1; //accelerate
    initialBackground.speedUpdate(speed);
    finalBackground.speedUpdate(speed);
    enemy.speedUpEnemy(speed);
  }
  if (key.keyCode == 40) { //down key
    speed -= 1; //deccelerate
    if (speed <= 0) {
      speed = 1;
    }
    initialBackground.speedUpdate(speed);
    finalBackground.speedUpdate(speed);
    enemy.speedUpEnemy(speed);
  }

  if (key.keyCode == 37) { //left key
    player.positionLeft(); //shift left
  }

  if (key.keyCode == 39) { //right key
    player.positionRight(); //shift right
  }
}

window.addEventListener("keydown", keyPressed);

function animate() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  requestAnimationFrame(animate);
  initialBackground.updateBackground();
  finalBackground.updateBackground();
  checkCollision(enemy, player);
  player.showCar();
  enemy.enemyUpdate();
  scoreUpdate();
}

animate();
