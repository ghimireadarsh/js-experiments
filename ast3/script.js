var people = [{
    id: 1,
    name: "Aegon Targaryen",
    children: [{
        id: 2,
        name: "Jaehaerys Targaryen",
        children: [{
            id: 4,
            name: "Daenerys Targaryen"
        }, {
            id: 5,
            name: "Rhaegar Targaryen",
            children: [{
                id: 6,
                name: "Aegon Targaryen"
            }]
        }]
    }, {
        id: 3,
        name: "Rhaelle Targaryen"
    }],
}];

var normalizedOutput = {}; //object to store output
let countId = 1; //count the id
var findChildren = function (people) {
    var findChild = [];
    for (var i = 0 ; i < people.length ; i++) {
        findChild.push( people[i].id );
    }
    return findChild;
}

var normalize = function ( people ) {

    for (var i = 0 ; i < people.length ; i++) {
        var object = {};
        if ( people[i].hasOwnProperty('children') ) {
            object['id'] = people[i].id;
            object['name'] = people[i].name;
            object['children'] = findChildren(people[i].children);
            normalizedOutput[countId] = object;
            countId++;

            //recursive function
            normalize(people[i].children);
        } 
        else {
            people[i]['children'] = [];
            normalizedOutput[countId] = people[i];
            countId++;
        }
    }
}
normalize( people );
console.log( normalizedOutput );
