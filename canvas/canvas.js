var canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');


// c.fillStyle = 'rgba(255, 0, 0, 0.5)'; // to color the rectangle
// c.fillRect(0 , 0 , 100 , 100); // to draw the rectangle (x,y,width,height) (x and y are relative to position (0,0) 
// c.fillStyle = 'rgba(0, 255, 0, 0.5)';
// c.fillRect(1300 , 0 , 100 , 100); 
// c.fillStyle = 'rgba(0, 0, 255, 0.5)';
// c.fillRect(0 , 500 , 100 , 100);
// c.fillStyle = 'rgba(0, 255, 255, 0.5)';
// c.fillRect(1300 , 500 , 100 , 100);  

 

// console.log(canvas);

// //line
// c.beginPath();
// c.moveTo(50 , 50) ; // (x,y) start point
// c.lineTo(1350, 50); //(x,y) end point
// c.strokeStyle = 'white';
// c.moveTo(1350,50)
// c.lineTo(1350, 550);
// c.lineTo(50, 550);
// c.lineTo(50, 50);
// // //stroke method to insert line
// c.stroke();


//arc // circle

// c.beginPath();
// c.strokeStyle = 'yellow';
// c.arc(50 , 50 , 40 , 0 , Math.PI * 2 , false ); // (x, y , radius, start angle, end angle)
// c.stroke();

// c.beginPath(); // to seperate the arc from another arc else it creates line to connect them
// c.arc(1350 , 50 , 40 ,0, Math.PI * 2 , false );
// c.stroke();
// c.beginPath();
// c.arc(1350, 550, 40, 0, Math.PI * 2 , false );
// c.stroke();
// c.beginPath();
// c.arc(50 , 550, 40 , 0, Math.PI * 2 , false );
// c.stroke();


// for(var i = 0 ; i < 10 ; i++){
//     var x = Math.random() * 1250;
//     var y = Math.random() * 450;
//     c.beginPath();
//     c.arc(x , y , 30 , 0 , Math.PI * 2, false );
//     c.strokeStyle = 'blue';
//     c.stroke();
// }
// var x = Math.random()*innerWidth;
// var y = Math.random()*innerHeight;
// var dx = (Math.random() - 0.5)*20;
// var dy = (Math.random() - 0.5)*20;
// var radius = 50;

var mouse = {
    x : undefined,
    y : undefined
}
var maxRadius = 50;
var minRadius = 2;
var colorArray = [
    '#0F1E32',
    '#013859',
    '#FFFFF8',
    '#EB3E4A',
    '#21BEDA'
];

window.addEventListener('mousemove', function(event){
    //console.log(event);
    mouse.x = event.x;
    mouse.y = event.y;
    console.log(mouse);
})
window.addEventListener('resize', function(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

})

function Circle(x , y , radius , dx , dy ) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.minRadius = radius;
    this.color =colorArray[Math.floor(Math.random() * colorArray.length)];
    this.draw = function(){
        // console.log('adarsh');
        c.beginPath();
        c.arc(this.x , this.y , this.radius , 0 , Math.PI *2 , false);
        c.strokeStyle = 'white';
        c.stroke();
        c.fillStyle =  this.color;
        c.fill();

    }
    this.update = function(){
        if(this.x + this.radius  > innerWidth || this.x-this.radius < 0){
            this.dx= -this.dx;
        }
        if(this.y + this.radius  > innerHeight || this.y-this.radius < 0){
            this.dy= -this.dy;
        }
        this.x += this.dx;
        this.y += this.dy;
        
        //interact
        if(mouse.x - this.x < 50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50){
            if(this.radius < maxRadius){
                this.radius += 1;
            }
        }
        else if (this.radius > this.minRadius){
            this.radius -= 1;
        }

        this.draw();

    }
};

var numberOfCircles = 40;
var circleArray = [];
for (var i = 0; i<=numberOfCircles ; i++){
    
    var x = Math.random()*(innerWidth-radius*2) + radius;
    var y = Math.random()*(innerHeight-radius*2) + radius ;
    var dx = (Math.random() - 0.5)*8;
    var dy = (Math.random() - 0.5)*5;
    var radius = Math.random() * 10 + 3;
    circleArray.push(new Circle(x, y, radius, dx, dy));
}


function animate(){
    requestAnimationFrame(animate); //creates loop within the animate within
    //console.log('adarsh');
    c.clearRect(0 , 0 , innerWidth , innerHeight);
    // circle.update();
    for(var i = 0 ; i < circleArray.length ; i++ ){
        circleArray[i].update();
    }
   
    }

animate();